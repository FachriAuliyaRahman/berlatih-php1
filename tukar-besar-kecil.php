<pre>
<?php
function tukar_besar_kecil($string)
{
    $a = strtoupper($string);
    $b = strtolower($string);
    $c = ucfirst($string);
    $d = ucwords($string);
    echo $string;
    print("\n");
    print("\n");
    print("string menjadi kapital semua= > $a");
    print("\n");
    print("\n");
    print("string menjadi huruf kecil semua= > $b");
    print("\n");
    print("\n");
    print("huruf pertama string akan menjadi besar= > $c");
    print("\n");
    print("\n");
    print("huruf pertama setiap kata akan string akan menjadi besar= > $d");
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"
